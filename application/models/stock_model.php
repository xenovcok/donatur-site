<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Stock_model extends CI_Model {

	public function __construct() {
		parent::__construct();
		$this->load->database();
	}
	
	public function get_all () {
		$this->db->select('stocks.*, items.name');
		$this->db->from('stocks');
		$this->db->join('items', 'stocks.item = items.id');
		$query = $this->db->get();

		if($query) {
			return $query->result();
		}	

		return null;

	}

	public function add($data) {
		return $this->db->insert('stocks', $data);
	}

	public function get_by_order_type ($data) {
		$sql = "SELECT *, SUM(quantity) as 'stock', `items`.`name` as 'item_name' FROM transactions LEFT 	JOIN items
				ON `transactions`.`item` = `items`.`id` 
				WHERE order_type = ? AND status = 'done' GROUP BY item
				";
		$bind = array($data);
		$query = $this->db->query($sql, $bind);

		if($query){
			return $query->result();
		}

		return null;
	}

	public function get_stock_by_owner ($data) {
		$sql = "SELECT `stocks`.*, `items`.`name` 
				FROM `stocks` 
				JOIN `items`
				ON `stocks`.`item` = `items`.`id`
				WHERE `stocks`.`owner`= ?";
		$bind = array($data);
		$query = $this->db->query($sql, $bind);

		if($query) {
			return $query->result();
		}	

		return null;
	}

	public function get_count_item_with_owner ($id,$data) {
		$sql = "SELECT `stocks`.*, `items`.`name` 
				FROM `stocks` 
				JOIN `items`
				ON `stocks`.`item` = `items`.`id`
				WHERE `stocks`.`owner`= ?
				AND `stocks`.`item`= ?";
		$bind = array($data, $id);
		$query = $this->db->query($sql, $bind);

		if($query) {
			return $query->num_rows();
		}	

		return null;
	}

	public function get_stock_item_by_order_type($data) {
		$sql = "SELECT *, SUM(quantity) as 'stock', `items`.`name` as 'item_name' FROM transactions LEFT 	JOIN items
				ON `transactions`.`item` = `items`.`id` 
				WHERE order_type = ? 
                AND status = 'done' 
                AND `items`.`id` = ? 
                GROUP BY item";

        $bind = array($data);
		$query = $this->db->query($sql, $bind);

		if($query){
			return $query->result();
		}

		return null;
	}

	public function get_by_id ($id) {
		$query = $this->db->get_where('stocks', array('id' => $id));

		if($query) {
			return $query->row();
		}

		return null;
	}

	public function get_by_item ($id) {
		$query = $this->db->get_where('stocks', array('item' => $id));

		if($query) {
			return $query->row();
		}

		return null;
	}

	public function get_by_item_and_owner($item, $owner) {
		$query = $this->db->get_where('stocks', array('item' => $item, 'owner' => $owner));

		if($query) {
			return $query->row();
		}

		return null;
	}

	public function edit($id, $data) {

		$this->db->where('id', $id);

		return $this->db->update('stocks', $data);
	}

	public function edit_with_owner($id, $owner, $data) {
		$this->db->where('item', $id);
		$this->db->where('owner', $owner);

		return $this->db->update('stocks', $data);
	}

	public function edit_stock($id, $data) {
		$this->db->where('item', $id);

		return $this->db->update('stocks', $data);
	}
																							
	public function delete($id) {
		$this->db->where('id', $id);

		return $this->db->delete('stocks');
	}

	public function get_total_items() {
		$query = $this->db->get('stocks');

		if($query) {
			return $query->num_rows();
		}

		return null;	
	}

	public function get_stock_details ($user) {
		$sql = "SELECT u.f_name,i.name,t1.item,t1.total_diterima,t2.total_terpakai
				from 
				(
				SELECT *, sum(quantity) as total_diterima from transactions WHERE order_type = 'user_request' and status = 'done' and user = ? group by item
				) as t1
				LEFT JOIN
				(
				SELECT user,item,sum(quantity) as total_terpakai
				from transactions
				where order_type = 'for_use'
				and status = 'done'
				and user = ?
				group by item
				) as t2
				ON t1.item = t2.item
				JOIN users u
				ON t1.user = u.id
				JOIN items i
				ON t1.item = i.id";
		$bind = array($user, $user);
		$query = $this->db->query($sql, $bind);

		if($query) {
			return $query->result();
		}

		return null;
	}	
}