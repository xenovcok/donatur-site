<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Transaction_model extends CI_Model {

	public function __construct() {
		parent::__construct();
		$this->load->database();
	}
	
	public function get_all () {
		$sql = 'SELECT `items`.`name` as item_name,`transactions`.* FROM `items` JOIN `transactions` ON `items`.`id` = `transactions`.`item`';
		$query = $this->db->query($sql);

		if($query) {
			return $query->result();
		}	

		return null;

	}

	public function get_by_order ($tipe) {
		$sql = 'SELECT `transactions`.*, `items`.`name` as `item_name`, `users`.`f_name` as `donatur`
				FROM `transactions`
				JOIN `items`
				ON `transactions`.`item` = `items`.`id`
				LEFT JOIN `users`
				ON `transactions`.`user` = `users`.`id` 
				WHERE `transactions`.`order_type` = ?';
		$bind = array($tipe);
		$query = $this->db->query($sql, $bind);

		if($query) {
			return $query->result();
		}	

		return null;
	}

	public function get_by_order_and_id ($tipe,$id) {
		$sql = 'SELECT `transactions`.*, `items`.`name` as `item_name`, `users`.`f_name` as `donatur`
				FROM `transactions`
				JOIN `items`
				ON `transactions`.`item` = `items`.`id`
				LEFT JOIN `users`
				ON `transactions`.`user` = `users`.`id` 
				WHERE `transactions`.`order_type` = ?
				AND `transactions`.`user` = ?';
		$bind = array($tipe,$id);
		$query = $this->db->query($sql, $bind);

		if($query) {
			return $query->result();
		}	

		return null;
	}

	public function get_by_order_status ($tipe,$status) {
		$sql = 'SELECT `transactions`.*, `items`.`name` as `item_name`, `users`.`f_name` as `donatur`
				FROM `transactions`
				JOIN `items`
				ON `transactions`.`item` = `items`.`id`
				LEFT JOIN `users`
				ON `transactions`.`user` = `users`.`id` 
				WHERE `transactions`.`order_type` = ?
				AND `transactions`.`status` = ?
				ORDER BY `transactions`.`updated_at`';
		$bind = array($tipe,$status);
		$this->db->order_by('created_at', 'desc');
		$query = $this->db->query($sql, $bind);

		if($query) {
			return $query->result();
		}	

		return null;
	}

	public function add($data) {
		return $this->db->insert('transactions', $data);
	}

	public function get_by_id ($id) {
		$query = $this->db->get_where('transactions', array('id' => $id));

		if($query) {
			return $query->row();
		}

		return null;
	}

	public function proses ($id, $jenis) {
		$this->db->where('id', $id);
		if($jenis == "done") {
			return $this->db->update('transactions', array('status' => $jenis, 'updated_at' => date('Y-m-d H:i:s') ));
		}

		return $this->db->update('transactions', array('status' => $jenis));
	}

	public function edit($id, $data) {

		$this->db->where('id', $id);

		return $this->db->update('transactions', $data);
	}
																							
	public function delete($id) {
		$this->db->where('id', $id);

		return $this->db->delete('transactions');
	}

	public function get_total_items() {
		$this->db->where('status', 'new');
		$query = $this->db->get('transactions');

		if($query) {
			return $query->num_rows();
		}

		return null;	
	}

	public function get_total_by_tipe($tipe) {
		$this->db->where('status', 'new');
		$this->db->where('order_type', $tipe);
		$query = $this->db->get('transactions');

		if($query) {
			return $query->num_rows();
		}

		return null;	
	}

	public function get_user_only() {
		$sql = "SELECT DISTINCT user as 'user_id' FROM transactions";

		$query = $this->db->query($sql);

		if($query) {
			return $query->result();
		}

		return null;
	}
}