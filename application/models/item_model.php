<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Item_model extends CI_Model {

	public function __construct() {
		parent::__construct();
		$this->load->database();
	}
	
	public function get_all () {
		$sql = "SELECT i.id,i.name,i.description,j.name as 'category'
		FROM items i JOIN category j
		ON i.category = j.id";

		$query = $this->db->query($sql);

		if($query) {
			return $query->result();
		}	

		return null;

	}

	public function get_wh_all () {
		$sql = "SELECT `items`.`id`,`items`.`name`, `stocks`.`item` 
				FROM `items`
				JOIN `stocks`
				ON `items`.`id`= `stocks`.`item`
				WHERE `stocks`.`owner` = 22";

		$query = $this->db->query($sql);

		if($query) {
			return $query->result();
		}

		return null;
	}

	public function add($data) {
		return $this->db->insert('items', $data);
	}

	public function get_by_id ($id) {
		$query = $this->db->get_where('items', array('id' => $id));

		if($query) {
			return $query->row();
		}

		return null;
	}

	public function edit($id, $data) {

		$this->db->where('id', $id);

		return $this->db->update('items', $data);
	}

	public function get_by_owner($owner) {
		$sql = "SELECT `items`.`id`,`items`.`name`
				FROM `items`
				JOIN `stocks`
				ON `items`.`id` = `stocks`.`item`
				WHERE `stocks`.`owner` = ?";

		$bind = array($owner);
		$query = $this->db->query($sql, $bind);

		if($query) {
			return $query->result();
		}

		return null;

	}
																							
	public function delete($id) {
		$this->db->where('id', $id);

		return $this->db->delete('items');
	}

	public function get_total_items() {
		$query = $this->db->get('items');

		if($query) {
			return $query->num_rows();
		}

		return null;	
	}
}