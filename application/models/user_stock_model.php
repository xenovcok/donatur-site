<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class User_stock_model extends CI_Model {

	public function __construct() {
		parent::__construct();
		$this->load->database();
	}
	
	public function getAll () {
		$query = $this->db->get('user_stock');

		if($query) {
			return $query->result();
		}	

		return null;

	}

	public function add($data) {
		return $this->db->insert('user_stock', $data);
	}

	public function getById ($id) {
		$query = $this->db->get_where('user_stock', array('id' => $id));

		if($query) {
			return $query->row();
		}

		return null;
	}

	public function edit($id, $data) {

		$this->db->where('user_stock', $id);

		return $this->db->update('user_stock', $data);
	}
																							
	public function delete($id) {
		$this->db->where('user_stock', $id);

		return $this->db->delete('user_stock');
	}
}