<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Category_model extends CI_Model {

	public function __construct() {
		parent::__construct();
		$this->load->database();
	}
	
	public function get_all () {
		$this->db->order_by('created_at', 'desc');
		$query = $this->db->get('category');

		if($query) {
			return $query->result();
		}	

		return null;

	}

	public function add($data) {
		return $this->db->insert('category', $data);
	}

	public function get_by_id ($id) {
		$query = $this->db->get_where('category', array('id' => $id));

		if($query) {
			return $query->row();
		}

		return null;
	}

	public function edit($id, $data) {

		$this->db->where('id', $id);

		return $this->db->update('category', $data);
	}
																							
	public function delete($id) {
		$this->db->where('id', $id);

		return $this->db->delete('category');
	}
}