<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends CI_Model {

	public function __construct() {
		parent::__construct();
		$this->load->database();
	}
	
	public function getAll () {
		$this->db->order_by('created_at', 'asc');
		$query = $this->db->get('users');

		if($query) {
			return $query->result();
		}	

		return null;

	}

	public function add($data) {
		return $this->db->insert('users', $data);
	}

	public function get_by_id ($id) {
		$query = $this->db->get_where('users', array('id' => $id));

		if($query) {
			return $query->row();
		}

		return null;
	}

	public function edit($id, $data) {

		$this->db->where('id', $id);

		return $this->db->update('users', $data);
	}
																							
	public function delete($id) {
		$this->db->where('id', $id);

		return $this->db->delete('users');
	}

	public function get_with_role($data) {

		$sql = 'SELECT * FROM users WHERE role = ? AND is_active = ? ORDER BY created_at DESC';
		$binds = array($data['role'],1);
		$query = $this->db->query($sql, $binds);

		$result = $query->result();

		return $result;
	}

	public function get_total_donatur() {

		$sql = 'SELECT * FROM users WHERE role = ? AND is_active = ?';
		$binds = array(2,1);
		$query = $this->db->query($sql, $binds);

		$result = $query->num_rows();

		return $result;
	}

	public function get_total_rs() {

		$sql = 'SELECT * FROM users WHERE role = ? AND is_active = ?';
		$binds = array(1,1);
		$query = $this->db->query($sql, $binds);

		$result = $query->num_rows();

		return $result;
	}

	public function get_last_id() {
		return $this->db->insert_id();
	}
}