<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login_model extends CI_Model {

	public function __construct() {
		parent::__construct();

		$this->load->database();
		$this->load->helper('date');
		$this->load->model('user_model');
	}
	
	public function login($data) {

		$sql = 'SELECT * FROM users WHERE username = ? AND is_active = ?';
		$binds = array($data['username'],1);
		$query = $this->db->query($sql, $binds);

		$options = array('cost' => 12);
		$password_hash = password_hash($data['password'], PASSWORD_BCRYPT, $options);
		if ($query->num_rows() > 0) {
			$rw_password = $query->result();
			if(password_verify($data['password'],$rw_password[0]->password)){
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	public function read_user_information($username) {
			
		$sql = 'SELECT * FROM users WHERE username = ?';
		$binds = array($username);
		$query = $this->db->query($sql, $binds);
		
		if ($query->num_rows() > 0) {
			return $query->result()[0];
		} else {
			return false;
		}
	}


	public function update_last_login($id, $ipaddress) {

		$data = array(
			'user' => $id,
			'ipaddress' => $ipaddress
		);

		$query = $this->db->insert('login_history', $data);

		if($query) {
			return true;
		}else{
			return false;
		}
	}

	public function logout() {
		return $this->session->sess_destroy();
	}
}