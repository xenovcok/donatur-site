<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Client_Controller extends MY_Controller {

	public function __construct() {
		parent::__construct();

		$this->load->library('session');
		$this->load->helper(array(
			'url',
			'form'
		));

		if($this->session->userdata('user_role') != 1) {
			redirect(base_url());
		}

	}
} 