<main id="main">

	<!-- ======= Get Started Section ======= -->
	<section id="get-started" class="padd-section text-center wow fadeInUp">

		<div class="container">
			<div class="section-title text-center mt-3">

				<h2>Selamat Datang</h2>
				<p class="separator">Bali Spirit Island</p>

			</div>
		</div>

		<div class="container">
			<div class="row">

				<div class="col-md-6 col-lg-4 offset-md-4">
					<div class="feature-block">
						<h4>Salurkan Bantuan Anda!</h4>
						<p>Website penyaluran donasi BPBD Bali</p>

					</div>
				</div>

			</div>
		</div>

		<div class="container">
			<div class="row">

				<div class="col-md-6 col-lg-6">
					<div class="card">
						<div class="card-body">
							<h5 class="card-title">Histori Donasi</h5>
							<table class="table table-hover table-custom table_histori_donasi">
								<thead>
									<tr>
										<th scope="col">Donatur</th>
										<th scope="col">Barang</th>
										<th scope="col">Jumlah</th>
										<th scope="col">Tanggal Diterima</th>
									</tr>
								</thead>
								<tbody>
									<?php foreach ($last_donatur as $key => $value) { ?>		
									<tr>
										<td scope="row"><?php echo $value->donatur; ?></td>
										<td><?php echo $value->item_name; ?></td>
										<td><?php echo $value->quantity; ?></td>
										<td><?php echo $value->updated_at; ?></td>
									</tr>
									<?php } ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>

				<div class="col-md-6 col-lg-6">
					<div class="card">
						<div class="card-body">
							<h5 class="card-title">Histori Pemakaian RS</h5>
							<table class="table table-hover table-custom table_stock_details">
								<thead>
									<tr>
										<th scope="col">RS</th>
										<th scope="col">Barang</th>
										<th scope="col">Diterima</th>
										<th scope="col">Terpakai</th>
										<th scope="col">Sisa</th>
									</tr>
								</thead>
								<tbody>
									<?php foreach ($stock_details as $key => $value) {
										foreach ($value as $key => $val) {
									?>
									<tr>
										<th scope="row"><?php echo $val->f_name; ?></th>
										<td><?php echo $val->name; ?></td>
										<td>
											<?php 
												if($val->total_diterima==0) { 
													echo 0; 
												}else{ 
													echo $val->total_diterima; 
												} 
											?>
										</td>
										<td>
											<?php 
												if($val->total_terpakai==0) { 
													echo 0; 
												}else{ 
													echo $val->total_terpakai; 
												} 
											?>
										</td>
										<td><?php echo $val->total_diterima-$val->total_terpakai; ?></td>
									</tr>
									<?php 
											}
										}
									?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>

		</section><!-- End Get Started Section -->
		<section class="text-center wow fadeInUp mb-5">
			<div class="container">
				<div class="row">
					<div class="col-md-6 col-lg-12">
						<div class="card">
							<div class="card-body">
								<h5 class="card-title">Kebutuhan Belum Terpenuhi</h5>
								<table class="table table-hover table-custom table_stock_request">
									<thead>
										<tr>
											<th scope="col">RS</th>
											<th scope="col">Barang</th>
											<th scope="col">Jumlah</th>
											<th scope="col">Ket</th>
										</tr>
									</thead>
									<tbody>
										<?php foreach ($user_request as $key => $value) { ?>
										<tr>
											<th scope="row"><?php echo $value->donatur; ?></th>
											<td><?php echo $value->item_name; ?></td>
											<td><?php echo $value->quantity; ?></td>
											<td><?php echo $value->description; ?></td>
										</tr>
										<?php
											}
										?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<section id="contact" class="padd-section wow fadeInUp">

	      <div class="container">
	        <div class="section-title text-center">
	          <p class="separator"><a href="<?php echo base_url().'page/donate'; ?>" class="btn btn-lg btn-dark">FORM DONASI</a></p>
	        </div>
	      </div>
	  </section>
	</main>