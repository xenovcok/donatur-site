<section id="get-started" class="padd-section wow fadeInUp">

	<div class="container text-center">
		<div class="section-title text-center mt-3">

			<h2>Form Donasi </h2>
			<p class="separator">Bali Spirit Island</p>

		</div>
	</div>

	<div class="container">
		<div class="row text-center">

			<div class="col-md-6 col-lg-6 offset-md-3 offset-lg-3">
				<div class="feature-block">
					<h4>Silahkan isi data diri Anda</h4>
					<p></p>

				</div>
			</div>

		</div>
	</div>

	<div class="container">
		<div class="row">
			<div class="col-md-6 offset-md-3 col-lg-6">
				<div class="card">
					<div class="card-body">
						<?php echo form_open(base_url().'donatur/item/add_order', 'id="form_order_donatur"'); ?>
						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<label for="donatur_name">Nama Lengkap</label>
									<input type="text" id="donatur_name" name="donatur_name" class="form-control" required>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label for="email">Email</label>
									<input type="email" id="email" name="donatur_email" class="form-control" placeholder="contoh@bali.com" required="true">
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label for="phone">No. Handphone</label>
									<input type="number" id="phone" name="donatur_phone" class="form-control" placeholder="08xxxxxxxxxx" required="true">
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<label for="address">Alamat</label>
									<textarea id="address" name="address" class="form-control"></textarea>
								</div>
							</div>
						</div>
						<div class="row text-center">
							<div class="col-md-12">
								<h4>Barang donasi</h4>
							</div>
							
						</div>
						<div class="row mb-1">
							<div class="col-md-6">
								<div class="form-group">
									<label for="item">Jenis Barang</label>
									<select id="item" name="item_id" class="form-control">
										<option value="0" disabled selected></option>
										<?php foreach ($items as $key => $value) { ?>
										<option value="<?php echo $value->id; ?>"><?php echo $value->name; ?></option>
										<?php } ?>
									</select>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label for="jumlah">Jumlah</label>
									<input type="number" id="jumlah" name="jumlah" class="form-control" required="true">
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<label for="ket">Keterangan</label>
									<textarea id="ket" name="keterangan" class="form-control"></textarea>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<button type="submit" id="donatur_submit" class="btn btn-primary float-right">Kirim</button>
							</div>
						</div>
						<?php echo form_close(); ?>
					</div>
				</div>
			</div>
		</div>

</section><!-- End Get Started Section -->