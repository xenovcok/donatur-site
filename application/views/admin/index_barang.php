<section class="content">
  <!-- modal -->
  <div class="modal fade" id="conf-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Hapus data?</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </div>
        <div class="modal-body alert alert-danger">
          <?php echo form_open(base_url().'admin/item/delete/', 'id="delete_form"'); ?>
          <?php echo form_close(); ?>
          <p>Data yang telah terhapus tidak bisa dikembalikan</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary" id="btn-yes">Delete</button>
        </div>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->
  <!-- edit modal -->
  <div class="modal fade" id="edit-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Edit Barang</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-12 col-md-12 col-sm-12">
              <?php echo form_open(base_url().'admin/item/edit', 'id = "edit_user_form"'); ?>
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label for="item_name">Nama Barang</label>
                    <input type="text" id="edit_name" name="edit_name" class="form-control" placeholder="Nama Barang">
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-8">
                  <div class="form-group">
                    <label for="item_category">Jenis</label>
                    <select id="edit_category" name="edit_category" class="form-control" placeholder="Jenis Barang">
                      <option value="uncategory" selected disabled>Pilih Jenis</option>
                      <?php foreach ($category as $key => $value) { ?>
                        <option value="<?php echo $value->id; ?>"><?php echo $value->name; ?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>
                <!-- <div class="col-md-4">
                  <div class="form-group">
                    <label for="jumlah">Jumlah</label>
                    <input type="number" id="edit_jumlah" name="edit_stock" class="form-control" placeholder="Jumlah Barang">
                </div>
              </div> -->
            </div>
            <div class="row">
              <div class="col-12 col-md-12">
                <div class="form-group">
                  <label for="edit_deskripsi">Deskripsi</label>
                  <textarea name="edit_description" id="edit_deskripsi" class="form-control"></textarea>
                </div>
              </div>
            </div>
            <div clas="row">
              <div class="col-12 col-md-12 col-sm-12">
                <button id="edit_item_btn" type="submit" class="btn btn-primary float-right btn-sm">Simpan</button>
              </div>
            </div>
            <?php echo form_close(); ?>
          </div>
        </div>
      </div>
      <div class="modal-footer" hidden="true">
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
  <!-- edit modal -->
</div>
<div class="modal fade" id="edit-stok-modal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="overlay d-none justify-content-center align-items-center">
        <i class="fas fa-2x fa-sync fa-spin"></i>
      </div>
      <div class="modal-header">
        <h4 class="modal-title">Tambah Stok</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-12 col-md-12 col-sm-12">
            <?php echo form_open(base_url().'admin/stock/tambah', 'id = "edit_stok_form"'); ?>
              <div class="form-group">
                <label for="jumlah">Stok</label>
                <div class="input-group">
                  <input type="hidden" id="hidden_stok_edit" name="old_stock" class="form-control">
                  <input type="number" id="stok_edit" name="edit_stock" class="form-control" placeholder="0">
                  <div class="input-group-append">
                    <button type="submit" class="btn btn-primary form-control" placeholder="0">Tambah</input>
                  </div>
                </div>
              </div>              
            <?php echo form_close(); ?>
          </div>
        </div>
      </div>
      <div class="modal-footer">
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div>
<!-- end edit modal -->
<div class="container-fluid">
  <div class="row">
    <div class="col-12 col-sm-6 col-md-3">
      <div class="info-box">
        <span class="info-box-icon bg-info elevation-1"><i class="fas fa-cog"></i></span>
        <div class="info-box-content">
          <span class="info-box-text">Total Barang</span>
          <span class="info-box-number">
            <?php echo $total; ?>
          </span>
        </div>
        <!-- /.info-box-content -->
      </div>
      <!-- /.info-box -->
    </div>
    <!-- /.col -->
  </div>
  <div class="row">
    <div class="col-12 col-sm-12 col-md-12">
      <div class="card collapsed-card bg-gradient-white">
        <div class="card-header border-0 d-flex align-items-center">
          <h3 class="card-title">
            <i class="fas fa-map-marker-alt mr-1"></i>
            Barang Donasi
          </h3>
          <!-- card tools -->
          <div class="card-tools ml-auto">
            <button type="button"
            class="btn btn-primary btn-sm daterange"
            data-toggle="tooltip"
            title="Date range">
            <i class="far fa-calendar-alt"></i>
          </button>
          <button type="button"
          class="btn btn-primary btn-sm"
          data-card-widget="collapse"
          data-toggle="tooltip"
          title="Collapse">
          <i class="fas fa-plus mr-1"></i>
          Tambah
        </button>
      </div>
      <!-- /.card-tools -->
    </div>
    <div class="card-body">
      <div class="row">
        <!-- open form add item -->
        <div class="col-8 col-md-8 col-sm-12">
          <h4>Tambah Barang</h4>
          <?php echo form_open(base_url().'admin/item/add', 'id = "form_add_item"'); ?>
          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <label for="item_name">Nama Barang</label>
                <input type="text" id="item_name" name="item_name" class="form-control" placeholder="Nama Barang">
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-8">
              <div class="form-group">
                <label for="item_category">Jenis</label>
                <select id="item_category" name="item_category" class="form-control" placeholder="Jenis Barang">
                  <option value="uncategory" selected disabled>Pilih Jenis</option>
                  <?php foreach ($category as $key => $value) { ?>
                    <option value="<?php echo $value->id; ?>"><?php echo $value->name; ?></option>
                  <?php } ?>
                </select>
              </div>
            </div>
          </div>
        <div class="row">
          <div class="col-12 col-md-12">
            <div class="form-group">
              <label for="deskripsi_item">Deskripsi</label>
              <textarea name="item_description" id="deskripsi_item" class="form-control"></textarea>
            </div>
          </div>
        </div>
        <div clas="row">
          <div class="col-12 col-md-12 col-sm-12">
            <button id="add_item_btn" type="submit" class="btn btn-primary float-right btn-sm">Simpan</button>
          </div>
        </div>
        <?php echo form_close(); ?>
      </div>
      <div class="col-4 col-md-4 col-sm-12">
        <h4>Jenis Barang</h4>
        <?php echo form_open(base_url().'admin/category/add', 'id = "add_category_form"'); ?>
        <div clas="row">
          <div class="col-12 col-md-12 col-sm-12">
            <div class="form-group">
              <label for="category_name">Nama Jenis</label>
              <input type="text" id="category_name" class="form-control" name="category_name" placeholder="nama Jenis">
            </div>
          </div>
        </div>
        <div clas="row">
          <div class="col-12 col-md-12 col-sm-12">
            <div class="form-group">
              <label for="category_deskripsi">Keterangan</label>
              <input type="text" id="category_deskripsi" class="form-control" name="category_deskripsi" placeholder="Keterangan">
            </div>
          </div>
        </div>
        <div clas="row">
          <div class="col-12 col-md-12 col-sm-12">
            <button id="add_category_btn" type="submit" class="btn btn-primary float-right btn-sm">Simpan</button>
          </div>
        </div>
        <?php form_close(); ?>
      </div>
      <!-- end form add item -->
    </div>
  </div>
  <!-- /.card-body-->
</div>
</div>
</div>
<div class="row">
  <div class="col-12 col-sm-12 col-md-12">
    <div class="card">
      <div class="card-header">
              <!-- <div class="row">
                <div class="col-12 col-md-6 col-sm-3 d-flex align-items-center">
                  <h3 class="card-title">List Barang Donasi</h3>
                </div>
                <div class="col-12 col-md-6 col-sm-3">
                  <a href="<?php echo base_url().'admin/item/tambah'; ?>" class="btn btn-sm btn-primary float-right">Tambah</a>
                </div>
              </div>       -->        
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="table_barang" class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th>ID</th>
                    <th>Nama Barang</th>
                    <th>Keterangan</th>
                    <th>Jenis Barang</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  <?php 
                  foreach ($item as $key => $val) {
                    ?>
                    <tr>
                      <td>
                        <strong><?php echo $val->id; ?></strong>
                      </td>
                      <td>
                        <?php echo $val->name; ?>
                      </td>
                      <td>
                        <?php echo $val->description; ?>
                      </td>
                      <td> 
                        <?php echo $val->category; ?>
                      </td>
                      <td>
                        <button data-id="<?php echo $val->id; ?>" class="btn btn-sm btn-warning edit-item" ><i class="fa fas fa-edit"></i></button>
                        <button class="btn btn-sm btn-danger delete" type="button" data-toggle="tooltip" data-id="<?php echo $val->id; ?>" title="Delete" url="<?php echo base_url().'admin/item/delete/'; ?>"><i class="fa fas fa-trash"></i></button>
                        <button class="btn btn-sm btn-primary add" type="button" data-toggle="tooltip" data-id="<?php echo $val->id; ?>" title="Tambah Stok"><i class="fa fas fa-plus mr-1"></i>Stok</button>
                      </td>
                    </tr>
                    <?php 
                  } 
                  ?>
                </tbody>
                <!-- <tfoot>
                <tr>
                  <th>Rendering engine</th>
                  <th>Browser</th>
                  <th>Platform(s)</th>
                  <th>Engine version</th>
                  <th>CSS grade</th>
                </tr>
              </tfoot> -->
            </table>
          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.info-box -->
      </div>
      <!-- /.col -->
    </div>
  </div>
</section>