<section class="content">
  <!-- modal -->
  <div class="modal fade" id="conf-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Hapus data?</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </div>
        <div class="modal-body alert alert-danger">
          <?php echo form_open(base_url().'admin/rs/delete/', 'id="delete_form"'); ?>
          <?php echo form_close(); ?>
          <p>Data yang telah terhapus tidak bisa dikembalikan</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary" id="btn-yes">Delete</button>
        </div>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->
  <!-- edit modal -->
  <div class="modal fade" id="edit-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Edit Donatur</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-12 col-md-12 col-sm-12">
              <?php echo form_open(base_url().'admin/rs/edit', 'id="edit_user_form"'); ?>
              <div class="row">
                <div class="col-6 col-md-6 col-sm-12">
                  <div class="form-group">
                    <label for="user_name">User</label>
                    <input type="text" name="username" autocomplete="off" id="edit_username" class="form-control" placeholder="Nama"/>
                  </div>
                </div>
                <div class="col-6 col-md-6 col-sm-12">
                  <div class="form-group">
                    <label for="f_name">Nama Lengkap</label>
                    <input type="text" id="edit_f_name" name="f_name" class="form-control" placeholder="Andi"/>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-6 col-md-6 col-sm-12">
                  <div class="form-group">
                    <label for="email">Email</label>
                    <input type="email" id="edit_email" name="email" class="form-control" placeholder="email@contoh.com"/>
                  </div>
                </div>
                <div class="col-6 col-md-6 col-sm-12">
                  <div class="form-group">
                    <label for="phone">No. Handphone</label>
                    <input type="number" id="edit_phone" name="phone" class="form-control" placeholder="087xxxxxxxxx"/>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <label for="alamat">Alamat</label>
                <textarea id="edit_alamat" name="address" class="form-control"></textarea>
              </div>
              <div clas="row">
              <div class="col-12 col-md-12 col-sm-12">
                <button id="edit_submit" type="submit" class="btn btn-primary float-right btn-sm">Simpan</button>
              </div>
            </div>
              <?php echo form_close(); ?>
            </div>
          </div>
        </div>
        <div class="modal-footer" hidden="true">
        </div>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div>
  <!-- end edit modal -->
	<div class="container-fluid">
		<div class="row">
      <div class="col-12 col-sm-6 col-md-3">
        <div class="info-box">
          <span class="info-box-icon bg-info elevation-1"><i class="fas fa-cog"></i></span>

          <div class="info-box-content">
            <span class="info-box-text">Total User / RS</span>
            <span class="info-box-number">
              <?php echo $total; ?>
            </span>
          </div>
          <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
      </div>
      <!-- /.col -->
    </div>
    <div class="row">
      <div class="col-12 col-sm-12 col-md-12">
        <div class="card collapsed-card bg-gradient-white">
          <div class="card-header border-0 d-flex align-items-center">
            <h3 class="card-title">
              <i class="fas fa-users mr-1"></i>
              User RS
            </h3>
            <!-- card tools -->
            <div class="card-tools ml-auto">
              <button type="button"
              class="btn btn-primary btn-sm"
              data-card-widget="collapse"
              data-toggle="tooltip"
              title="Collapse">
              <i class="fas fa-plus mr-1"></i>
              Tambah
            </button>
          </div>
          <!-- /.card-tools -->
        </div>
        <div class="card-body">
          <div class="row">
            <div class="col-6 col-md-6 col-sm-12">
              <?php echo form_open(base_url().'admin/rs/add', 'id="add_user_form"'); ?>
              <h4>Tambah User / RS</h4>
              <div class="row">
                <div class="col-6 col-md-6 col-sm-12">
                  <div class="form-group">
                    <label for="user_name">User</label>
                    <input type="text" name="username" autocomplete="off" id="edit_username" class="form-control" placeholder="Nama"/>
                  </div>
                </div>
                <div class="col-6 col-md-6 col-sm-12">
                  <div class="form-group">
                    <label for="f_name">Nama Lengkap</label>
                    <input type="text" id="edit_f_name" name="f_name" class="form-control" placeholder="Andi"/>
                  </div>
                </div>
              </div>
               <div class="row">
                <div class="col-6 col-md-6 col-sm-12">
                  <div class="form-group">
                    <label for="pass">Password</label>
                    <div class="input-group">
                      <input type="password" autocomplete="off" name="password" id="pass" class="form-control" placeholder="Password" required="" />
                      <div class="input-group-append">
                        <button id="btn_show_pass" class="form-control btn btn-outline-info"><i class="fas fa-eye"></i></button>
                        <button id="btn_pass_generator" class="form-control btn btn-info">Generate</button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-6 col-md-6 col-sm-12">
                  <div class="form-group">
                    <label for="email">Email</label>
                    <input type="email" id="email" name="email" class="form-control" placeholder="email@contoh.com" required />
                  </div>
                </div>
                <div class="col-6 col-md-6 col-sm-12">
                  <div class="form-group">
                    <label for="phone">No. Handphone</label>
                    <input type="number" id="phone" name="phone" class="form-control" placeholder="087xxxxxxxxx" required />
                  </div>
                </div>
              </div>
              <div class="form-group">
                <label for="alamat">Alamat</label>
                <textarea id="alamat" name="address" class="form-control"></textarea>
              </div>
              <div clas="row">
              <div class="col-12 col-md-12 col-sm-12">
                <button id="add_user_btn" type="submit" class="btn btn-primary float-right btn-sm">Simpan</a>
              </div>
            </div>
              <?php echo form_close(); ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-12 col-sm-12 col-md-12">
      <div class="card">
        <div class="card-header border-0 d-flex align-items-center">
          
        <!-- /.card-tools -->
        </div>
      <!-- /.card-header -->
      <div class="card-body">
        <table id="table_rs" class="table table-bordered table-striped">
          <thead>
            <tr>
              <th>#</th>
              <th>Nama</th>
              <th>Email</th>
              <th>Telepon</th>
              <th>Alamat</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            <?php 
            $i=1;
            foreach ($user as $key => $val) {
              ?>
              <tr>
                <td>
                  <strong><?php echo $i; ?></strong>
                </td>
                <td>
                  <?php echo $val->f_name; ?>
                </td>
                <td>
                  <?php echo $val->email; ?>
                </td>
                <td> 
                  <?php echo $val->phone; ?>
                </td>
                <td>
                  <?php echo $val->address; ?>
                </td>
                <td>
                  <button class="btn btn-sm btn-warning edit-rs" data-toggle="tooltip" title="Edit" data-id="<?php echo $val->id; ?>"><i class="fa fas fa-edit" data-id="<?php echo $val->id; ?>"></i></button>
                  <button class="btn btn-sm btn-danger delete" type="button" data-toggle="tooltip" title="Delete" data-id="<?php echo $val->id; ?>" url="<?php echo base_url().'admin/rs/delete/'; ?>"><i class="fa fas fa-trash"></i></button>
                </td>
              </tr>
              <?php 
              $i++;
            } 
            ?>
          </tbody>
                <!-- <tfoot>
                <tr>
                  <th>Rendering engine</th>
                  <th>Browser</th>
                  <th>Platform(s)</th>
                  <th>Engine version</th>
                  <th>CSS grade</th>
                </tr>
              </tfoot> -->
            </table>
          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.info-box -->
      </div>
      <!-- /.col -->
    </div>
  </div>
</section>