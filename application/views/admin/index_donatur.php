<section class="content">
  <!-- modal -->
  <div class="modal fade" id="conf-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Hapus data?</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </div>
        <div class="modal-body alert alert-danger">
          <?php echo form_open(base_url().'admin/donatur/delete/', 'id="delete_form"'); ?>
          <?php echo form_close(); ?>
          <p>Data yang telah terhapus tidak bisa dikembalikan</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary" id="btn-yes">Delete</button>
        </div>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->
  <!-- edit modal -->
  <div class="modal fade" id="edit-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Edit Donatur</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-12 col-md-12 col-sm-12">
              <?php echo form_open(base_url().'admin/donatur/edit', 'id="edit_user_form"'); ?>
              <div class="form-group">
                <label for="user_name">Nama Donatur</label>
                <input type="text" name="username" autocomplete="off" id="edit_username" class="form-control" placeholder="Nama"/>
              </div>
              <!-- <div class="form-group">
                <label for="pass">Password</label>
                <input type="password" autocomplete="off" name="password" id="pass" class="form-control" placeholder="Password"/>
              </div> -->
              <div class="row">
                <div class="col-6 col-md-6 col-sm-12">
                  <div class="form-group">
                    <label for="email">Email</label>
                    <input type="email" id="edit_email" name="email" class="form-control" placeholder="email@contoh.com"/>
                  </div>
                </div>
                <div class="col-6 col-md-6 col-sm-12">
                  <div class="form-group">
                    <label for="phone">No. Handphone</label>
                    <input type="number" id="edit_phone" name="phone" class="form-control" placeholder="087xxxxxxxxx"/>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <label for="alamat">Alamat</label>
                <textarea id="edit_alamat" name="address" class="form-control"></textarea>
              </div>
              <div clas="row">
              <div class="col-12 col-md-12 col-sm-12">
                <button id="edit_submit" type="submit" class="btn btn-primary float-right btn-sm">Simpan</button>
              </div>
            </div>
              <?php echo form_close(); ?>
            </div>
          </div>
        </div>
        <div class="modal-footer" hidden="true">
        </div>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div>
  <!-- end edit modal -->
	<div class="container-fluid">
		<div class="row">
      <div class="col-12 col-sm-6 col-md-3">
        <div class="info-box">
          <span class="info-box-icon bg-info elevation-1"><i class="fas fa-cog"></i></span>

          <div class="info-box-content">
            <span class="info-box-text">Total Donatur</span>
            <span class="info-box-number">
              <?php echo $total; ?>
            </span>
          </div>
          <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
      </div>
      <!-- /.col -->
    </div>
    <div class="row">
      <div class="col-12 col-sm-12 col-md-12">
        <div id="add_user_card" class="card collapsed-card bg-gradient-white">
          <div class="card-header border-0 d-flex align-items-center">
            <h3 class="card-title">
              <i class="fas fa-users mr-1"></i>
              Donatur
            </h3>
            <!-- card tools -->
            <div class="card-tools ml-auto">
              <button type="button"
              class="btn btn-primary btn-sm"
              data-card-widget="collapse"
              data-toggle="tooltip"
              title="Collapse">
              <i class="fas fa-plus mr-1"></i>
              Tambah
            </button>
          </div>
          <!-- /.card-tools -->
        </div>
        <div class="card-body">
          <div class="row">
            <div class="col-6 col-md-6 col-sm-12">
              <?php echo form_open(base_url().'admin/donatur/add', 'id="add_user_form"'); ?>
              <h4>Tambah Donatur</h4>
              <div class="form-group">
                <label for="user_name">Nama Donatur</label>
                <input type="text" name="username" autocomplete="off" id="user_name" class="form-control" placeholder="Nama"/>
              </div>
              <!-- <div class="form-group">
                <label for="pass">Password</label>
                <input type="password" autocomplete="off" name="password" id="pass" class="form-control" placeholder="Password"/>
              </div> -->
              <div class="row">
                <div class="col-6 col-md-6 col-sm-12">
                  <div class="form-group">
                    <label for="email">Email</label>
                    <input type="email" id="email" name="email" class="form-control" placeholder="email@contoh.com"/>
                  </div>
                </div>
                <div class="col-6 col-md-6 col-sm-12">
                  <div class="form-group">
                    <label for="phone">No. Handphone</label>
                    <input type="number" id="phone" name="phone" class="form-control" placeholder="087xxxxxxxxx"/>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <label for="alamat">Alamat</label>
                <textarea id="alamat" name="address" class="form-control"></textarea>
              </div>
              <div clas="row">
              <div class="col-12 col-md-12 col-sm-12">
                <button id="add_user_btn" type="submit" class="btn btn-primary float-right btn-sm">Simpan</button>
              </div>
            </div>
              <?php echo form_close(); ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
    <div class="row">
      <div class="col-12 col-sm-12 col-md-12">
        <div class="card">
            <!-- <div class="card-header">
              <h3 class="card-title">Donatur List</h3>
            </div> -->
            <!-- /.card-header -->
            <div class="card-body">
              <table id="table_donatur" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>#</th>
                  <th>Nama</th>
                  <th>Email</th>
                  <th>No. Handphone</th>
                  <th>Alamat</th>
                  <th>Tanggal Donasi</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <?php 
                  $i=1;
                  foreach ($donatur as $key => $val) {
                ?>
                <tr>
                  <td>
                    <strong><?php echo $i; ?></strong>
                  </td>
                  <td>
                    <?php echo $val->f_name; ?>
                  </td>
                  <td>
                    <?php echo $val->email; ?>
                  </td>
                  <td> 
                    <?php echo $val->phone; ?>
                  </td>
                  <td>
                    <?php echo $val->address; ?>
                  </td>
                  <td> 
                    <?php echo $val->created_at; ?>
                  </td>
                  <td>
                    <button class="btn btn-sm btn-warning edit" data-toggle="tooltip" data-id="<?php echo $val->id; ?>" title="Edit"><i class="fa fas fa-edit"></i></button>
                    <button class="btn btn-sm btn-danger delete" type="button" data-toggle="tooltip" data-id="<?php echo $val->id; ?>" title="Delete" url="<?php echo base_url().'admin/donatur/delete/'; ?>"><i class="fa fas fa-trash"></i></button>
                  </td>
                </tr>
                <?php 
                  $i++;
                } 
                ?>
                </tbody>
                <!-- <tfoot>
                <tr>
                  <th>Rendering engine</th>
                  <th>Browser</th>
                  <th>Platform(s)</th>
                  <th>Engine version</th>
                  <th>CSS grade</th>
                </tr>
                </tfoot> -->
              </table>
            </div>
            <!-- /.card-body -->
          </div>
        <!-- /.info-box -->
      </div>
      <!-- /.col -->
    </div>
  </div>
</section>