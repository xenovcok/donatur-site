<section class="content">
  <!-- delete modal -->
  <div class="modal fade" id="conf-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Hapus data?</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </div>
        <div class="modal-body alert alert-danger">
          <?php echo form_open(base_url().'admin/item/delete/', 'id="delete_form"'); ?>
          <?php echo form_close(); ?>
          <p>Data yang telah terhapus tidak bisa dikembalikan</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary" id="btn-yes">Delete</button>
        </div>
      </div>
    </div>
  </div>
  <!-- end delete modal -->
  <!-- edit modal -->
  <div class="modal fade" id="edit-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Edit Stock</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </div>
        <div class="modal-body">
          <?php echo form_open(base_url().'admin/category/edit/', 'id="edit_user_form"'); ?>
          <div clas="row">
            <div class="col-12 col-md-12 col-sm-12">
              <div class="form-group">
                <label for="category_name">Nama Jenis</label>
                <input type="text" id="edit_category_name" class="form-control" name="category_name" placeholder="nama Jenis">
              </div>
            </div>
          </div>
          <div clas="row">
            <div class="col-12 col-md-12 col-sm-12">
              <div class="form-group">
                <label for="category_deskripsi">Keterangan</label>
                <input type="text" id="edit_category_deskripsi" class="form-control" name="category_deskripsi" placeholder="Keterangan">
              </div>
            </div>
          </div>
          <div clas="row">
            <div class="col-12 col-md-12 col-sm-12">
              <button id="edit_category_btn" type="submit" class="btn btn-primary float-right btn-sm">Simpan</button>
            </div>
          </div>
          <?php echo form_close(); ?>
        </div>
        <div class="modal-footer">
        </div>
      </div>
    </div>
  </div>
  <!-- end edit modal -->
  <div class="row">
    <div class="col-12 col-sm-12 col-md-12">
      <div class="card">
        <div class="card-header">
              <!-- <div class="row">
                <div class="col-12 col-md-6 col-sm-3 d-flex align-items-center">
                  <h3 class="card-title">List Barang Donasi</h3>
                </div>
                <div class="col-12 col-md-6 col-sm-3">
                  <a href="<?php echo base_url().'admin/category/tambah'; ?>" class="btn btn-sm btn-primary float-right">Tambah</a>
                </div>
              </div>       -->        
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="table_category" class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Barang</th>
                    <th>Stok</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  <?php 
                  $i = 1;
                  foreach ($stocks as $key => $val) {
                    ?>
                    <tr>
                      <td>
                        <strong><?php echo $i; ?></strong>
                      </td>
                      <td>
                        <?php echo $val->name; ?>
                      </td>
                      <td>
                        <?php echo $val->quantity; ?>
                      </td>
                      <td>
                        <button data-id="<?php echo $val->id; ?>" class="btn btn-sm btn-warning edit-category" ><i class="fa fas fa-edit"></i></button>
                        <button class="btn btn-sm btn-danger delete" type="button" data-toggle="tooltip" data-id="<?php echo $val->id; ?>" title="Delete" url="<?php echo base_url().'admin/category/delete/'; ?>"><i class="fa fas fa-trash"></i></button>
                      </td>
                    </tr>
                    <?php $i++;
                  } 
                  ?>
                </tbody>
                <!-- <tfoot>
                <tr>
                  <th>Rendering engine</th>
                  <th>Browser</th>
                  <th>Platform(s)</th>
                  <th>Engine version</th>
                  <th>CSS grade</th>
                </tr>
              </tfoot> -->
            </table>
          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.info-box -->
      </div>
      <!-- /.col -->
    </div>
  </div>
</section>