<footer class="footer">
  <div class="container">
    <div class="row">

      <div class="col-md-12 col-lg-4">
        <div class="footer-logo">

          <a class="navbar-brand" href="#">Balispiritisland.com</a>
          <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the.</p>

        </div>
      </div>

      <div class="col-sm-6 col-md-3 col-lg-2">
        <div class="list-menu">

          <h4>Berita</h4>

          <ul class="list-unstyled">
            <li><a href="#">Darurat</a></li>
            <li><a href="#">Serangan covid19</a></li>
            <li><a href="#">Live streaming</a></li>
            <li><a href="#">Privacy Policy</a></li>
          </ul>

        </div>
      </div>

      <div class="col-sm-6 col-md-3 col-lg-2">
        <div class="list-menu">

          <h4>Agenda</h4>

          <ul class="list-unstyled">
            <li><a href="#">About us</a></li>
            <li><a href="#">Features item</a></li>
            <li><a href="#">Live streaming</a></li>
            <li><a href="#">Privacy Policy</a></li>
          </ul>

        </div>
      </div>

      <div class="col-sm-6 col-md-3 col-lg-2">
        <div class="list-menu">

          <h4>Support</h4>

          <ul class="list-unstyled">
            <li><a href="#">faq</a></li>
            <li><a href="#">Editor help</a></li>
            <li><a href="#">Contact us</a></li>
            <li><a href="#">Privacy Policy</a></li>
          </ul>

        </div>
      </div>

      <div class="col-sm-6 col-md-3 col-lg-2">
        <div class="list-menu">

          <h4>Abou Us</h4>

          <ul class="list-unstyled">
            <li><a href="#">About us</a></li>
            <li><a href="#">Features item</a></li>
            <li><a href="#">Live streaming</a></li>
            <li><a href="#">Privacy Policy</a></li>
          </ul>

        </div>
      </div>

    </div>
  </div>

  <div class="copyrights">
    <div class="container">
      <p>&copy; Copyrights eStartup. All rights reserved.</p>
      <div class="credits">
      Designed by DariBootstrap</a>
    </div>
  </div>
</div>

</footer><!-- End  Footer -->

<a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>

<!-- Vendor JS Files -->
<script src="<?php echo base_url(); ?>assets/vendor/jquery/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="<?php echo base_url(); ?>assets/vendor/jquery.easing/jquery.easing.min.js"></script>
<script src="<?php echo base_url(); ?>assets/vendor/php-email-form/validate.js"></script>
<script src="<?php echo base_url(); ?>assets/vendor/wow/wow.min.js"></script>
<script src="<?php echo base_url(); ?>assets/vendor/modal-video/js/modal-video.min.js"></script>
<script src="<?php echo base_url(); ?>assets/vendor/owl.carousel/owl.carousel.min.js"></script>
<script src="<?php echo base_url(); ?>assets/vendor/superfish/superfish.min.js"></script>
<script src="<?php echo base_url(); ?>assets/vendor/hoverIntent/hoverIntent.js"></script>
<script src="<?php echo base_url(); ?>assets/adminlte/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="<?php echo base_url(); ?>assets/adminlte/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?php echo base_url(); ?>assets/adminlte/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<!-- Toastr -->
<script src="<?php echo base_url();?>assets/adminlte/plugins/toastr/toastr.min.js"></script>

<!-- Template Main JS File -->
<script src="<?php echo base_url(); ?>assets/js/home/main.js"></script>

</body>

</html>