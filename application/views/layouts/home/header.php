<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Bali Spirit Island - App</title>
  <meta content="" name="descriptison">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="assets/img/favicon.png" rel="icon">
  <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Roboto:100,300,400,500,700|Philosopher:400,400i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="<?php echo base_url(); ?>assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>assets/vendor/animate.css/animate.min.css" rel="stylesheet">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/adminlte/plugins/fontawesome-free/css/all.min.css">
  <link href="<?php echo base_url(); ?>assets/vendor/modal-video/css/modal-video.min.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>assets/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">
  <!-- toaster -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/adminlte/plugins/toastr/toastr.min.css">

  <link href="<?php echo base_url(); ?>assets/css/style.css" rel="stylesheet">

</head>

<body>

  <!-- ======= Header ======= -->
  <header id="header" class="header header-hide pb-4">
    <div class="container">

      <div id="logo" class="pull-left d-inline">
        <!-- <h1><a href="#body" class="scrollto"><span>Bali</span>Spiritisland</a></h1> -->
        <!-- Uncomment below if you prefer to use an image logo -->
         <a href="#"><img src="<?php echo base_url(); ?>assets/img/logo.png" alt="" title="" /></a>
      </div>

      <nav id="nav-menu-container">
        <ul class="nav-menu">
          <li class="menu-active"><a href="<?php echo base_url(); ?>">Home</a></li>
          <li><a href="<?php echo base_url().'page/donate'; ?>"><i class="fas fa-heart mr-1 text-danger"></i>Donasi</a></li>
          <li><a href="#">Berita</a></li>
          <li><a href="#">Agenda</a></li>
          <li><a href="<?php echo base_url().'login'; ?>" class="btn-light">Login</a></li>
          <!-- <li class="menu-has-children"><a href="">Drop Down</a>
            <ul>
              <li><a href="#">Drop Down 1</a></li>
              <li><a href="#">Drop Down 3</a></li>
              <li><a href="#">Drop Down 4</a></li>
              <li><a href="#">Drop Down 5</a></li>
            </ul>
          </li>
          <li><a href="#blog">Blog</a></li>
          <li><a href="#contact">Contact</a></li> -->
        </ul>
      </nav><!-- #nav-menu-container -->
    </div>
  </header>