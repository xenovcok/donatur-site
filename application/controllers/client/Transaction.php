<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Transaction extends Client_Controller {

	public function __construct() {
		parent::__construct();

		$this->load->model('transaction_model');
		$this->load->model('item_model');
		$this->load->model('stock_model');
	}

	public function index() {
		$h_data['title'] = 'Permintaan User/RS';
		$data['total'] = $this->transaction_model->get_total_by_tipe('user_request');
		$data['transactions'] = $this->transaction_model->get_by_order('user_request');
		$h_data['total_to_wh'] = $data['total'];
		$data['items'] = $this->item_model->get_all();
		$data['trans_type'] = 'user_request';

		$this->load->view('client/header', $h_data);
		$this->load->view('client/index_transaction', $data);
		$this->load->view('client/footer');
	}

	public function pemakaian() {
		$h_data['title'] = 'Pemakaian Barang';
		$data['total'] = $this->transaction_model->get_total_by_tipe('for_use');
		$h_data['total'] = $data['total'];
		$data['transactions'] = $this->transaction_model->get_by_order_and_id('for_use',$this->session->userdata('user_id'));
		$data['items'] = $this->item_model->get_all();
		$data['trans_type'] = 'for_use';

		$this->load->view('client/header', $h_data);
		$this->load->view('client/index_transaction', $data);
		$this->load->view('client/footer');
	}

	public function retreive() {
		$h_data['title'] = 'Barang Diterima';
		$data['total'] = $this->transaction_model->get_total_by_tipe('user_request');
		$h_data['total'] = $data['total'];
		$data['transactions'] = $this->transaction_model->get_by_order_and_id('user_request', $this->session->userdata('user_id'));
		$data['items'] = $this->item_model->get_all();
		$data['trans_type'] = 'user_request';

		$this->load->view('client/header', $h_data);
		$this->load->view('client/transaction_retreive', $data);
		$this->load->view('client/footer');
	}

	public function add($order_type) {
		
		$data = array(
			'user' => $this->session->userdata('user_id'),
			'item' => $this->input->post('item_id'),
			'quantity' => $this->input->post('item_qty'),
			'description' => $this->input->post('description'),
			'order_type' => $order_type,
			'status' => 'new'
		);

		$result = $this->transaction_model->add($data);

		if($result) {
			$response = array('message'=>'ok', 'status' => 201);
			$this->output
				->set_status_header(201)
				->set_content_type('application/json')
				->set_output(json_encode($response));
		}else{
			$response = array('message'=>'failed', 'status' => 204);
			$this->output
				->set_status_header(201)
				->set_content_type('application/json')
				->set_output(json_encode($response));
		}
	}

	public function add_with_donatur() {
		
		$data = array(
			'item' => $this->input->post('item_id'),
			'quantity' => $this->input->post('item_qty'),
			'user' => $this->input->post('donatur_id'),
			'order_type' => 'from_donatur',
			'status' => 'new'
		);

		$result = $this->transaction_model->add($data);

		if($result) {
			$response = array('message'=>'ok', 'status' => 201);
			$this->output
				->set_status_header(201)
				->set_content_type('application/json')
				->set_output(json_encode($response));
		}else{
			$response = array('message'=>'failed', 'status' => 204);
			$this->output
				->set_status_header(201)
				->set_content_type('application/json')
				->set_output(json_encode($response));
		}
	}


	public function proses($id, $jenis) {
		$result = $this->transaction_model->proses($id, $jenis);

		if($result) {
			$response = array('message'=>'ok', 'status' => 201);
			$this->output
				->set_status_header(201)
				->set_content_type('application/json')
				->set_output(json_encode($response));
		}else{
			$response = array('message'=>'failed', 'status' => 204);
			$this->output
				->set_status_header(201)
				->set_content_type('application/json')
				->set_output(json_encode($response));
		}
	}

	public function delete ($id) {
		$result = $this->transaction_model->delete($id);

		if($result) {
			$response = array('message'=>'ok', 'status' => 201);
			$this->output
				->set_status_header(201)
				->set_content_type('application/json')
				->set_output(json_encode($response));
		}else{
			$response = array('message'=>'failed', 'status' => 204);
			$this->output
				->set_status_header(201)
				->set_content_type('application/json')
				->set_output(json_encode($response));
		}
	}

	public function proses_done($id, $jenis) {
		$result = $this->transaction_model->proses($id, $jenis);

		$item_id = $this->input->post('item_id');
		$tipe = $this->input->post('tipe');
		$owner_id = $this->session->userdata('user_id');

		$transaction = $this->transaction_model->get_by_id($id);

		$stock_item_exist = $this->stock_model->get_count_item_with_owner($item_id, $owner_id);

		$result2 = NULL;

		if($stock_item_exist > 0) {
			$data = array();
			$res = $this->stock_model->get_by_item_and_owner($item_id, $owner_id);

			$old_qty = $res->quantity;

			if($tipe=="for_use") {
				$data = array(
					'quantity' => $old_qty-$transaction->quantity
				);
			}else{
				$data = array(
					'quantity' => $old_qty+$transaction->quantity
				);
			}

			$result2 = $this->stock_model->edit_with_owner($item_id, $owner_id, $data);
		}else{
			$data = array(
				'item' => $item_id,
				'owner' => $owner_id,
				'quantity' => $transaction->quantity
			);

			$result2 = $this->stock_model->add($data);
		}
		// $item_qty = $result_item->quantity+$result_trans->quantity;

		// $data = array(
		// 	'quantity' => $item_qty
		// );
		
		if($result && $result2) {
			$response = array('data'=> $result2,'message'=>'ok', 'status' => 201);
			$this->output
				->set_status_header(201)
				->set_content_type('application/json')
				->set_output(json_encode($response));
		}else{
			$response = array('message'=>'failed', 'status' => 204);
			$this->output
				->set_status_header(201)
				->set_content_type('application/json')
				->set_output(json_encode($response));
		}
	}
}