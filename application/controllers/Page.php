<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Page extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct() {
		parent::__construct();

		$this->load->library('session');
		$this->load->helper(array(
			'url',
			'form'
		));

		$this->load->model('transaction_model');
		$this->load->model('item_model');
		$this->load->model('stock_model');
	}

	public function index()
	{
		// $role = $this->session->userdata('user_role');
		
		// if(empty($this->session->userdata('username'))) {
		// 	$this->load->view('index_login');
		// }else{
		// 	if($role == 45) {
		// 		redirect(base_url().'admin/dashboard');
		// 	}else if($role == 46) {
		// 		redirect(base_url().'warehouse/dashboard');
		// 	}else if($role == 1) {
		// 		redirect(base_url().'client/dashboard');
		// 	}
		// }

		$data['last_donatur'] = $this->transaction_model->get_by_order_status('from_donatur', 'done');
		$data['user_request'] = $this->transaction_model->get_by_order_status('user_request', 'new');
		
		$user_list = $this->transaction_model->get_user_only();
		$container = array();

		// $stock = array();

		foreach ($user_list as $key => $value) {
			if($value->user_id!=null) {
				array_push($container, $this->stock_model->get_stock_details($value->user_id));
			} 
		}

		$data['stock_details'] = array_filter($container);

		$this->load->view('layouts/home/header');
		$this->load->view('landing', $data);
		$this->load->view('layouts/home/footer');
		
	}

	public function donate() {
		$data['items'] = $this->item_model->get_all();

		$this->load->view('layouts/home/header');
		$this->load->view('donatur/donate_page', $data);
		$this->load->view('layouts/home/footer');
	}
}
