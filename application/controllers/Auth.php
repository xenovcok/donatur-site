<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

	public function __construct() {
		parent::__construct();

		$this->load->helper(array('url', 'form'));
		$this->load->library(array('form_validation', 'session'));

		$this->load->model(array('user_model', 'Login_model'));
		$this->load->database();
	}

	public function login() {
		
		$this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean');
		$this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean');

		$username = $this->input->post('username');
		$password = $this->input->post('password');

		$data = array(
			'username' => $username, 
			'password' => $password
		);

		$ipaddress = $this->input->ip_address();

		$result = $this->Login_model->login($data);

		if($result) {

			$user_details = $this->Login_model->read_user_information($username);

			$this->session->set_userdata('username', $user_details->username);
			$this->session->set_userdata('user_id', $user_details->id);
			$this->session->set_userdata('user_role', $user_details->role);

			$this->Login_model->update_last_login($user_details->id, $ipaddress);

			$response = array('role'=> $user_details->role, 'message'=>'ok', 'status' => 201);
			
			$this->output
				->set_status_header(201)
				->set_content_type('application/json')
				->set_output(json_encode($response));
				
		}else{
			$response = array('message'=>'Login gagal!', 'status' => 201);
			$this->output
				->set_status_header(201)
				->set_content_type('application/json')
				->set_output(json_encode($response));
		}
	}

	public function logout() {
		$this->Login_model->logout();

		$response = array('message'=>'ok', 'status' => 201);
		$this->output
			->set_status_header(201)
			->set_content_type('application/json')
			->set_output(json_encode($response));
	}
}