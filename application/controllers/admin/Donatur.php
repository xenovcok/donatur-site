<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Donatur extends Admin_Controller {
	public function __construct() {
		parent::__construct();

		$this->load->database();
		$this->load->helper(array( 
			'form'
		));
		$this->load->model('user_model');
	}

	public function index() {
		$h_data['title'] = 'Donatur';
		$data['total'] = $this->user_model->get_total_donatur();
		$data['donatur'] = $this->user_model->get_with_role(array('role'=> 2));

		$this->load->view('layouts/admin/header', $h_data);
		$this->load->view('admin/index_donatur', $data);
		$this->load->view('layouts/admin/footer');
	}

	public function add() {

		$data = array(
			'f_name' => $this->input->post('username'),
			'email' => $this->input->post('email'),
			'phone' => $this->input->post('phone'),
			'address' => $this->input->post('address'),
			'role' => 2
		);

		$result = $this->user_model->add($data);

		if($result) {
			$response = array('message'=>'ok', 'status' => 201);
			$this->output
				->set_status_header(201)
				->set_content_type('application/json')
				->set_output(json_encode($response));
		}else{
			$response = array('message'=>'failed', 'status' => 204);
			$this->output
				->set_status_header(201)
				->set_content_type('application/json')
				->set_output(json_encode($response));
		}

	}

	public function get_donatur() {
		$data = array(
			'role' => 2
		);

		$result = $this->user_model->get_with_role($data);

		if($result) {
			$response = array('data' => $result, 'message'=>'ok', 'status' => 201);
			$this->output
				->set_status_header(201)
				->set_content_type('application/json')
				->set_output(json_encode($response));
		}else{
			$response = array('message'=>'failed', 'status' => 204);
			$this->output
				->set_status_header(201)
				->set_content_type('application/json')
				->set_output(json_encode($response));
		}
	} 

	public function delete($id) {
		$result = $this->user_model->delete($id);

		if($result) {
			$response = array('message'=>'ok', 'status' => 201);
			$this->output
				->set_status_header(201)
				->set_content_type('application/json')
				->set_output(json_encode($response));
		}else{
			$response = array('message'=>'failed', 'status' => 204);
			$this->output
				->set_status_header(201)
				->set_content_type('application/json')
				->set_output(json_encode($response));
		}
	}

	public function get($id) {
		$result = $this->user_model->get_by_id($id);

		if($result) {
			$response = array('data' => $result, 'message'=>'ok', 'status' => 201);
			$this->output
				->set_status_header(201)
				->set_content_type('application/json')
				->set_output(json_encode($response));
		}else{
			$response = array('message'=>'failed', 'status' => 204);
			$this->output
				->set_status_header(201)
				->set_content_type('application/json')
				->set_output(json_encode($response));
		}
	}

	public function edit ($id) {
		$data = array(
			'f_name' => $this->input->post('username'),
			'email' => $this->input->post('email'),
			'phone' => $this->input->post('phone'),
			'address' => $this->input->post('address'),
			'updated_at' => date('Y-m-d H:i:s')
		);

		$result = $this->user_model->edit($id, $data);

		if($result) {
			$response = array('message'=>'ok', 'status' => 201);
			$this->output
				->set_status_header(201)
				->set_content_type('application/json')
				->set_output(json_encode($response));
		}else{
			$response = array('message'=>'failed', 'status' => 204);
			$this->output
				->set_status_header(201)
				->set_content_type('application/json')
				->set_output(json_encode($response));
		}
	}
}