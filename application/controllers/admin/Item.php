<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Item extends Admin_Controller {

	public function __construct() {
		parent::__construct();

		$this->load->database();
		$this->load->helper(array( 
			'form'
		));

		$this->load->library('session');

		$this->load->model('item_model');
		$this->load->model('category_model');
		$this->load->model('stock_model');
	}

	public function index() {
		$h_data['title'] = 'Barang Donatur';
		$data['total'] = $this->item_model->get_total_items();
		$data['item'] = $this->item_model->get_all();
		$data['category'] = $this->category_model->get_all();

		$this->load->view('layouts/admin/header', $h_data);
		$this->load->view('admin/index_barang', $data);
		$this->load->view('layouts/admin/footer');
	}

	public function get_all() {
		$result = $this->item_model->get_all();

		if($result) {
			$response = array('data' => $result, 'message'=>'ok', 'status' => 201);
			$this->output
				->set_status_header(201)
				->set_content_type('application/json')
				->set_output(json_encode($response));
		}else{
			$response = array('message'=>'failed', 'status' => 204);
			$this->output
				->set_status_header(201)
				->set_content_type('application/json')
				->set_output(json_encode($response));
		}
	}

	public function get($id) {
		$result = $this->item_model->get_by_id($id);

		if($result) {
			$response = array('data' => $result, 'message'=>'ok', 'status' => 201);
			$this->output
				->set_status_header(201)
				->set_content_type('application/json')
				->set_output(json_encode($response));
		}else{
			$response = array('message'=>'failed', 'status' => 204);
			$this->output
				->set_status_header(201)
				->set_content_type('application/json')
				->set_output(json_encode($response));
		}
	}

	// tambah item donasi
	public function add() {
		
		$data = array(
			'name' => $this->input->post('item_name'),
			'category' => $this->input->post('item_category'),
			'description' => $this->input->post('item_description')
		);

		$result = $this->item_model->add($data);
		
		$id = $this->db->insert_id();

		$data_stock = array(
			'item' => $id,
			'owner' => $this->session->userdata('user_id')
		);

		$result2 = $this->stock_model->add($data_stock);

		if($result && $result2) {
			$response = array('data' => $id ,'message'=>'ok', 'status' => 201);
			$this->output
				->set_status_header(201)
				->set_content_type('application/json')
				->set_output(json_encode($response));
		}else{
			$response = array('message'=>'failed', 'status' => 204);
			$this->output
				->set_status_header(201)
				->set_content_type('application/json')
				->set_output(json_encode($response));
		}
	}

	// edit item
	public function edit ($id) {
		$data = array(
			'name' => $this->input->post('edit_name'),
			'category' => $this->input->post('edit_category'),
			'description' => $this->input->post('edit_description'),
			'updated_at' => date('Y-m-d H:i:s')
		);

		$result = $this->item_model->edit($id, $data);

		if($result) {
			$response = array('message'=>'ok', 'status' => 201);
			$this->output
				->set_status_header(201)
				->set_content_type('application/json')
				->set_output(json_encode($response));
		}else{
			$response = array('message'=>'failed', 'status' => 204);
			$this->output
				->set_status_header(201)
				->set_content_type('application/json')
				->set_output(json_encode($response));
		}
	}

	public function tambah ($id) {
		$old = $this->input->post('old_stock');
		$new = $this->input->post('edit_stock');

		$data = array(
			'stock' => $old+$new
		);

		$result = $this->item_model->edit($id, $data);

		if($result) {
			$response = array('message'=>'ok', 'status' => 201);
			$this->output
				->set_status_header(201)
				->set_content_type('application/json')
				->set_output(json_encode($response));
		}else{
			$response = array('message'=>'failed', 'status' => 204);
			$this->output
				->set_status_header(201)
				->set_content_type('application/json')
				->set_output(json_encode($response));
		}
	}

	public function delete($id) {
		$result = $this->item_model->delete($id);

		if($result) {
			$response = array('message'=>'ok', 'status' => 201);
			$this->output
				->set_status_header(201)
				->set_content_type('application/json')
				->set_output(json_encode($response));
		}else{
			$response = array('message'=>'failed', 'status' => 204);
			$this->output
				->set_status_header(201)
				->set_content_type('application/json')
				->set_output(json_encode($response));
		}
	}
}