<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Stock extends Admin_Controller {

	public function __construct() {
		parent::__construct();

		$this->load->database();
		$this->load->helper(array(
			'url', 
			'form'
		));
		$this->load->library('session');

		$this->load->model('stock_model');
	}

	public function index() {
		$h_data['title'] = 'Barang Donatur';
		$data['total'] = $this->stock_model->get_total_items();
		// $data['stocks'] = $this->stock_model->get_by_order_type('from_donatur');
		$data['stocks'] = $this->stock_model->get_stock_by_owner(3);
		
		$this->load->view('layouts/admin/header', $h_data);
		$this->load->view('admin/index_stock', $data);
		$this->load->view('layouts/admin/footer');
	}

	public function get($id) {
		$result = $this->stock_model->get_by_id($id);

		if($result) {
			$response = array('data' => $result, 'message'=>'ok', 'status' => 201);
			$this->output
				->set_status_header(201)
				->set_content_type('application/json')
				->set_output(json_encode($response));
		}else{
			$response = array('message'=>'failed', 'status' => 204);
			$this->output
				->set_status_header(201)
				->set_content_type('application/json')
				->set_output(json_encode($response));
		}
	}

	public function tambah ($id) {
		$old = $this->input->post('old_stock');
		$new = $this->input->post('edit_stock');

		$data = array(
			'quantity' => $old+$new
		);

		$result = $this->stock_model->edit_stock($id, $data);

		if($result) {
			$response = array('message'=>'ok', 'status' => 201);
			$this->output
				->set_status_header(201)
				->set_content_type('application/json')
				->set_output(json_encode($response));
		}else{
			$response = array('message'=>'failed', 'status' => 204);
			$this->output
				->set_status_header(201)
				->set_content_type('application/json')
				->set_output(json_encode($response));
		}
	}

	public function get_by_item($id) {
		$result = $this->stock_model->get_by_item($id);

		if($result) {
			$response = array('data' => $result, 'message'=>'ok', 'status' => 201);
			$this->output
				->set_status_header(201)
				->set_content_type('application/json')
				->set_output(json_encode($response));
		}else{
			$response = array('message'=>'failed', 'status' => 204);
			$this->output
				->set_status_header(201)
				->set_content_type('application/json')
				->set_output(json_encode($response));
		}
	}

	// tambah item donasi
	public function add() {
		
		$data = array(
			'item' => $this->input->post('item_name'),
			'quantity' => $this->input->post('item_category'),
			'created_at' => round(microtime(true)*1000),
			'updated_at' => null
		);

		$result = $this->item_model->add($data);

		if($result) {
			$this->session->set_flashdata('message', 'Data tersimpan!');
			redirect(base_url().'admin/stock');
		}else{
			$this->session->set_flashdata('message', 'Data gagal disimpan!');
			redirect(base_url().'admin/stock');
		}
	}
}