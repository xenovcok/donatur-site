<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category extends Admin_Controller {
	public function __construct() {
		parent::__construct();

		$this->load->database();
		$this->load->helper(array(
			'form'
		));
		$this->load->model('category_model');
	}

	public function index() {
		$h_data['title'] = 'Category';
		$data['category'] = $this->category_model->get_all();

		$this->load->view('layouts/admin/header', $h_data);
		$this->load->view('admin/index_category', $data);
		$this->load->view('layouts/admin/footer');
	}

	public function add() {

		$data = array(
			'name' => $this->input->post('category_name'),
			'description' => $this->input->post('category_deskripsi')
		);

		$result = $this->category_model->add($data);

		if($result) {
			$response = array('message'=>'ok', 'status' => 201);
			$this->output
				->set_status_header(201)
				->set_content_type('application/json')
				->set_output(json_encode($response));
		}else{
			$response = array('message'=>'failed', 'status' => 204);
			$this->output
				->set_status_header(201)
				->set_content_type('application/json')
				->set_output(json_encode($response));
		}

	}

	public function delete($id) {
		$result = $this->category_model->delete($id);

		if($result) {
			$response = array('message'=>'ok', 'status' => 201);
			$this->output
				->set_status_header(201)
				->set_content_type('application/json')
				->set_output(json_encode($response));
		}else{
			$response = array('message'=>'failed', 'status' => 204);
			$this->output
				->set_status_header(201)
				->set_content_type('application/json')
				->set_output(json_encode($response));
		}
	}

	public function get($id) {
		$result = $this->category_model->get_by_id($id);

		if($result) {
			$response = array('data' => $result, 'message'=>'ok', 'status' => 201);
			$this->output
				->set_status_header(201)
				->set_content_type('application/json')
				->set_output(json_encode($response));
		}else{
			$response = array('message'=>'failed', 'status' => 204);
			$this->output
				->set_status_header(201)
				->set_content_type('application/json')
				->set_output(json_encode($response));
		}
	}

	public function edit ($id) {
		$data = array(
			'name' => $this->input->post('category_name'),
			'description' => $this->input->post('category_deskripsi')
		);

		$result = $this->category_model->edit($id, $data);

		if($result) {
			$response = array('message'=>'ok', 'status' => 201);
			$this->output
				->set_status_header(201)
				->set_content_type('application/json')
				->set_output(json_encode($response));
		}else{
			$response = array('message'=>'failed', 'status' => 204);
			$this->output
				->set_status_header(201)
				->set_content_type('application/json')
				->set_output(json_encode($response));
		}
	}
}