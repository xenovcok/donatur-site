<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rs extends Admin_Controller {
	public function __construct() {
		parent::__construct();

		$this->load->database();
		$this->load->helper(array(
			'url',
			'form'
		));
		$this->load->model('user_model');
	}

	public function index() {
		$h_data['title'] = 'Rumah Sakit';
		$data['total'] = $this->user_model->get_total_rs();
		$data['user'] = $this->user_model->get_with_role(array('role'=> 1));

		$this->load->view('layouts/admin/header', $h_data);
		$this->load->view('admin/index_rs', $data);
		$this->load->view('layouts/admin/footer');
	}

	public function add() {

		$password = $this->input->post('password');

		$options = array('cost' => 12);
		$password_hash = password_hash($password, PASSWORD_BCRYPT, $options);

		$data = array(
			'username' => $this->input->post('username'),
			'f_name' => $this->input->post('f_name'),
			'password' => $password_hash,
			'email' => $this->input->post('email'),
			'phone' => $this->input->post('phone'),
			'address' => $this->input->post('address'),
			'role' => 1
		);

		$result = $this->user_model->add($data);

		if($result) {
			$response = array('message'=>'ok', 'status' => 201);
			$this->output
				->set_status_header(201)
				->set_content_type('application/json')
				->set_output(json_encode($response));
		}else{
			$response = array('message'=>'failed', 'status' => 204);
			$this->output
				->set_status_header(201)
				->set_content_type('application/json')
				->set_output(json_encode($response));
		}

	}

	public function get($id) {
		$result = $this->user_model->get_by_id($id);

		if($result) {
			$response = array('data' => $result, 'message'=>'ok', 'status' => 201);
			$this->output
				->set_status_header(201)
				->set_content_type('application/json')
				->set_output(json_encode($response));
		}else{
			$response = array('message'=>'failed', 'status' => 204);
			$this->output
				->set_status_header(201)
				->set_content_type('application/json')
				->set_output(json_encode($response));
		}
	}

	public function edit ($id) {
		$data = array(
			'username' => $this->input->post('username'),
			'f_name' => $this->input->post('f_name'),
			'email' => $this->input->post('email'),
			'phone' => $this->input->post('phone'),
			'address' => $this->input->post('address'),
			'updated_at' => date('Y-m-d H:i:s')
		);

		$result = $this->user_model->edit($id, $data);

		if($result) {
			$response = array('message'=>'ok', 'status' => 201);
			$this->output
				->set_status_header(201)
				->set_content_type('application/json')
				->set_output(json_encode($response));
		}else{
			$response = array('message'=>'failed', 'status' => 204);
			$this->output
				->set_status_header(201)
				->set_content_type('application/json')
				->set_output(json_encode($response));
		}
	}

	public function delete($id) {
		$result = $this->user_model->delete($id);

		if($result) {
			$response = array('message'=>'ok', 'status' => 201);
			$this->output
				->set_status_header(201)
				->set_content_type('application/json')
				->set_output(json_encode($response));
		}else{
			$response = array('message'=>'failed', 'status' => 204);
			$this->output
				->set_status_header(201)
				->set_content_type('application/json')
				->set_output(json_encode($response));
		}
	}
}