<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// include_once('core/Admin_Controller');

class Dashboard extends Admin_Controller {
	
	public function __construct() {
		parent::__construct();

		$this->load->helper('url');
		$this->load->library('session');
	}

	public function index () {

		$h_data['title'] = 'Dashboard';
		$h_data['name'] = $this->session->userdata('username');

		$this->load->view('layouts/admin/header', $h_data);
		$this->load->view('layouts/admin/dashboard');
		$this->load->view('layouts/admin/footer');
	}	

}