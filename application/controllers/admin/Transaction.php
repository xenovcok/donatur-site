<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Transaction extends Admin_Controller {

	public function __construct() {
		parent::__construct();

		$this->load->model('transaction_model');
		$this->load->model('item_model');
		$this->load->model('stock_model');
	}

	public function index() {
		$h_data['title'] = 'History Barang Dikirim';
		$data['total'] = $this->transaction_model->get_total_by_tipe('to_wh');
		$data['transactions'] = $this->transaction_model->get_by_order('to_wh');
		$h_data['total_to_wh'] = $data['total'];
		$data['items'] = $this->item_model->get_all();
		$data['trans_type'] = 'to_wh';

		$this->load->view('layouts/admin/header', $h_data);
		$this->load->view('admin/index_transaction', $data);
		$this->load->view('layouts/admin/footer');
	}

	public function pemakaian() {
		$h_data['title'] = 'Pemakaian Barang';
		$data['total'] = $this->transaction_model->get_total_by_tipe('for_use');
		$h_data['total'] = $data['total'];
		$data['transactions'] = $this->transaction_model->get_by_order('for_use');
		$data['items'] = $this->item_model->get_all();
		$data['trans_type'] = 'for_use';

		$this->load->view('layouts/admin/header', $h_data);
		$this->load->view('admin/index_transaction', $data);
		$this->load->view('layouts/admin/footer');
	}

	public function retreive() {
		$h_data['title'] = 'Barang Diterima';
		$data['total'] = $this->transaction_model->get_total_by_tipe('from_donatur');
		$h_data['total'] = $data['total'];
		$data['transactions'] = $this->transaction_model->get_by_order('from_donatur');
		$data['items'] = $this->item_model->get_all();
		$data['trans_type'] = 'from_donatur';

		$this->load->view('layouts/admin/header', $h_data);
		$this->load->view('admin/transaction_retreive', $data);
		$this->load->view('layouts/admin/footer');
	}

	public function add($order_type) {
		
		$data = array(
			'item' => $this->input->post('item_id'),
			'quantity' => $this->input->post('item_qty'),
			'order_type' => $order_type,
			'status' => 'new'
		);

		$result = $this->transaction_model->add($data);

		if($result) {
			$response = array('message'=>'ok', 'status' => 201);
			$this->output
				->set_status_header(201)
				->set_content_type('application/json')
				->set_output(json_encode($response));
		}else{
			$response = array('message'=>'failed', 'status' => 204);
			$this->output
				->set_status_header(201)
				->set_content_type('application/json')
				->set_output(json_encode($response));
		}
	}

	public function add_with_donatur() {
		
		$data = array(
			'item' => $this->input->post('item_id'),
			'quantity' => $this->input->post('item_qty'),
			'user' => $this->input->post('donatur_id'),
			'order_type' => 'from_donatur',
			'status' => 'new'
		);

		$result = $this->transaction_model->add($data);

		if($result) {
			$response = array('message'=>'ok', 'status' => 201);
			$this->output
				->set_status_header(201)
				->set_content_type('application/json')
				->set_output(json_encode($response));
		}else{
			$response = array('message'=>'failed', 'status' => 204);
			$this->output
				->set_status_header(201)
				->set_content_type('application/json')
				->set_output(json_encode($response));
		}
	}


	public function proses($id, $jenis) {
		$tipe = $this->input->post('tipe');
		// update status to proses
		$result = $this->transaction_model->proses($id, $jenis);
		$result2 = NULL;

		if($tipe=="to_wh") {
			// ambil item id
			$trans = $this->transaction_model->get_by_id($id);
			$item_id = $trans->item;
			$owner = $this->session->userdata('user_id');

			$stock = $this->stock_model->get_by_item_and_owner($item_id, $owner);
			// ambil stok terkini admin donatur		
			$old_qty = $stock->quantity;
			$stock_to_save = $old_qty - $trans->quantity;

			$data = array(
				'quantity' => $stock_to_save
			);

			$result2 = $this->stock_model->edit_with_owner($item_id,$owner, $data);

			// $datalog = array(
			// 	'old_stock' => $old_qty,
			// 	'new_stock' => $trans->quantity,
			// 	'stock_to_save' => $stock_to_save,
			// 	'stock_id' => $stock->id,
			// 	'owner_id' => $owner,
			// 	'the_data' => $data
			// );
		}else{
			$result2 = FALSE;
		}

		if($result && $result2) {
			$response = array('message'=>'ok', 'status' => 201);
			$this->output
				->set_status_header(201)
				->set_content_type('application/json')
				->set_output(json_encode($response));
		}else{
			$response = array('message'=>'failed', 'status' => 204);
			$this->output
				->set_status_header(201)
				->set_content_type('application/json')
				->set_output(json_encode($response));
		}
	}

	public function proses_done($id, $jenis) {
		$result = $this->transaction_model->proses($id, $jenis);

		$item_id = $this->input->post('item_id');
		$owner_id = $this->input->post('owner_id');

		$transaction = $this->transaction_model->get_by_id($id);

		$stock_item_exist = $this->stock_model->get_count_item_with_owner($item_id, $owner_id);

		$result2 = NULL;

		if($stock_item_exist > 0) {
			$res = $this->stock_model->get_by_item_and_owner($item_id, $owner_id);

			$old_qty = $res->quantity;

			$data = array(
				'quantity' => $old_qty+$transaction->quantity
			);

			$result2 = $this->stock_model->edit_with_owner($item_id, $owner_id, $data);
		}else{
			$data = array(
				'item' => $item_id,
				'owner' => $owner_id,
				'quantity' => $transaction->quantity
			);

			$result2 = $this->stock_model->add($data);
		}
		// $item_qty = $result_item->quantity+$result_trans->quantity;

		// $data = array(
		// 	'quantity' => $item_qty
		// );
		
		if($result && $result2) {
			$response = array('data'=> $result2,'message'=>'ok', 'status' => 201);
			$this->output
				->set_status_header(201)
				->set_content_type('application/json')
				->set_output(json_encode($response));
		}else{
			$response = array('message'=>'failed', 'status' => 204);
			$this->output
				->set_status_header(201)
				->set_content_type('application/json')
				->set_output(json_encode($response));
		}
	}
}