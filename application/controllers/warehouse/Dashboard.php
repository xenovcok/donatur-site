<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// include_once('core/Admin_Controller');

class Dashboard extends Warehouse_Controller {
	
	public function __construct() {
		parent::__construct();

		$this->load->helper('url');
		$this->load->library('session');
	}

	public function index () {

		$h_data['title'] = 'Admin Gudang';
		$h_data['name'] = $this->session->userdata('username');

		$this->load->view('warehouse/header', $h_data);
		$this->load->view('warehouse/dashboard');
		$this->load->view('warehouse/footer');
	}	

}