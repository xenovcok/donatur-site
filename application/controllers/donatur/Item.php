<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Item extends CI_Controller {

	public function __construct() {
		parent::__construct();

		$this->load->model('transaction_model');
		$this->load->model('user_model');

		$this->load->helper(array(
			'url'
		));
	}

	public function add_order() {
		$data_user = array(
			'f_name' => $this->input->post('donatur_name'),
			'address' => $this->input->post('address'),
			'email' => $this->input->post('donatur_email'),
			'phone' => $this->input->post('donatur_phone'),
			'role' => 2
		);

		$result = $this->user_model->add($data_user);

		$id = $this->user_model->get_last_id();

		$data_transaksi = array(
			'user' => $id,
			'item' => $this->input->post('item_id'),
			'quantity' => $this->input->post('jumlah'),
			'description' => $this->input->post('keterangan'),
			'order_type' => 'from_donatur'	
		);

		$result2 = $this->transaction_model->add($data_transaksi);


		if($result && $result2) {
			$response = array('message'=>'ok', 'status' => 201);
			$this->output
				->set_status_header(201)
				->set_content_type('application/json')
				->set_output(json_encode($response));
		}else{
			$response = array('message'=>'failed', 'status' => 204);
			$this->output
				->set_status_header(201)
				->set_content_type('application/json')
				->set_output(json_encode($response));
		}
	}	
}