<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
	
	public function __construct() {
		parent::__construct();

		$this->load->library('session');
		$this->load->helper(array(
			'url',
			'form'
		));
	}

	public function index() {

		$role = $this->session->userdata('user_role');
		
		if(empty($this->session->userdata('username'))) {
			$this->load->view('index_login');
		}else{
			if($role == 45) {
				redirect(base_url().'admin/dashboard');
			}else if($role == 46) {
				redirect(base_url().'warehouse/dashboard');
			}else if($role == 1) {
				redirect(base_url().'client/dashboard');
			}
		}
	}
}