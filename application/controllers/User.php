<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	public function __construct() {
		parent::__construct();

		$this->load->helper(array('form', 'url'));
		$this->load->model('user_model');
	}

	public function index() {
		$this->load->view('user_page');
	}

	public function register() {

		$password = $this->input->post('password');

		$options = array('cost' => 12);
		$password_hash = password_hash($password, PASSWORD_BCRYPT, $options);

		$data = array(
			'username' => $this->input->post('username'),
			'password' => $password_hash,
			'email' => $this->input->post('email'),
			'phone' => NULL,
			'address' => NULL
		);

		$query = $this->user_model->add($data);

		if($query) {
			$response = array('message'=>'ok', 'status' => 201);
			$this->output
				->set_status_header(201)
				->set_content_type('application/json')
				->set_output(json_encode($response));
		}
	} 
}