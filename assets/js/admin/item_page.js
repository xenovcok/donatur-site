$(document).ready(function() {
	$('#add_item_btn').submit(function(e) {
		var form = $(this);
		var url = form.attr('action');

		e.preventDefault();
		
		$.ajax({
			url: url,
			type: "POST",
			data: form.serialize(),
			success: function(e){
				console.log(e);
			},
			error: function(e){
				console.log(e.message);
			}
		});
	});
});