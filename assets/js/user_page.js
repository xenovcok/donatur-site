$(document).ready(function() {
	$('#register_form').submit(function(e) {
		e.preventDefault();
		var form = $(this);
		var url = form.attr('action');

		$.ajax({
			url: url,
			type: "POST",
			data: form.serialize(),
			success: function(data) {
				console.log(data);
				form.trigger('reset');
				toastr.success('Data tersimpan!');
			},
			error: function() {
				console.log('error');
			}
		})
	});
});