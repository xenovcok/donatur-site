// var base_url = "http://localhost/donat/";
var base_url = "https://balispiritisland.com/";

$(function () {

	$("#table_donatur").DataTable({
		"responsive": true,
		"autoWidth": false,
		"order": [[0, "desc"]],
	});

	$("#table_rs").DataTable({
		"responsive": true,
		"autoWidth": false,
		"order": [[0, "desc"]],
	});

	$("#table_barang").DataTable({
		"responsive": true,
		"autoWidth": false,
		"order": [[0, "desc"]],
	});

	$("#table_category").DataTable({
		"responsive": true,
		"autoWidth": false,
		"order": [[0, "asc"]],
	});

	$('#form_add_item').submit(function(e) {
		var form = $(this);
		var url = form.attr('action');

		e.preventDefault();

		$.ajax({
			url: url,
			type: "POST",
			data: form.serialize(),
			success: function(e){
				if(e.status==201){
					toastr.info("Data disimpan!");
					console.log(e.data);
					setTimeout(function() {
						window.location.reload();
					}, 2000);
				}else{
					toastr.error("Gagal menyimpan data!");
				}
				form.trigger('reset');
			},
			error: function(e){
				console.log(e.message);
			}
		});
	});

	$('#btn_pass_generator').click(function(e) {
		e.preventDefault();
		var p_string = Math.random().toString(24).slice(-8);
		$('#pass').val(p_string);
	});

	$('#add_user_form').submit(function(e) {
		e.preventDefault();
		var form = $(this);
		var url = form.attr('action');

		$.ajax({
			url: url,
			type: "POST",
			data: form.serialize(),
			success: function(res) {
				form.trigger('reset');
				toastr.success("Data Tersimpan!");
				setTimeout(function(){
					window.location.reload(true);
				}, 2000);
			},
			error: function(e) {
				toastr.info('Gagal Menyimpan Data!');
			}
		}); 
	});

	$('#add_order_form').submit(function(e) {
		e.preventDefault();
		var form = $(this);
		var url = form.attr('action')+'/to_wh';
		var av_stock = $('#av_stock').val();
		var jml = $('#jumlah').val();
		//console.log('ok?', parseInt(av_stock) > parseInt(jml));
		if(parseInt(av_stock) >= parseInt(jml)) {
			$.ajax({
				url: url,
				type: "POST",
				data: form.serialize(),
				success: function(res) {
					form.trigger('reset');
					toastr.success("Data Tersimpan!");
					setTimeout(function(){
						window.location.reload(true);
					}, 2000);
				},
				error: function(e) {
					toastr.info('Gagal Menyimpan Data!');
				}
			});
		}else{
			toastr.error('Jumlah melebihi stok');
			$('#jumlah').focus();
		}
	});

	// add new donatur item
	$('#add_order_ret_form').submit(function(e) {
		e.preventDefault();
		var form = $(this);
		var url = form.attr('action')+'/from_donatur';
		var jml = $('#jumlah').val();
		//console.log('ok?', parseInt(av_stock) > parseInt(jml));
		if(parseInt(jml) > 0) {
			$.ajax({
				url: url,
				type: "POST",
				data: form.serialize(),
				success: function(res) {
					form.trigger('reset');
					toastr.success("Data Tersimpan!");
					setTimeout(function(){
						window.location.reload(true);
					}, 2000);
				},
				error: function(e) {
					toastr.info('Gagal Menyimpan Data!');
				}
			});
		}else{
			toastr.error('Jumlah tidak boleh kosong');
			$('#jumlah').focus();
		}
	});

 	// edit user btn
 	$('.edit').click(function(e) {
 		e.preventDefault();
 		var id = $(this).attr('data-id');
 		var form = $('#edit_user_form');
 		var url = base_url+'admin/donatur/edit';
 		form.attr('action', url+'/'+id);
 		$('#edit-modal').modal('show');

 		$.ajax({
 			url: base_url+'admin/donatur/get/'+id,
 			type: "GET",
 			success: function (res) {
 				$('#edit_username').val(res.data.username);
 				$('#edit_email').val(res.data.email);
 				$('#edit_phone').val(res.data.phone);
 				$('#edit_alamat').val(res.data.address);
 			},
 			error: function (e) {

 			}
 		});

 		// console.log(id);
 	});

 	$('.add').click(function(e) {
 		e.preventDefault();
 		var id = $(this).attr('data-id');
 		var form = $('#edit_stok_form');
 		var url = base_url+'admin/stock/tambah';
 		form.attr('action', url+'/'+id);
 		$('#edit-stok-modal').modal('show');

 		$.ajax({
 			url: base_url+'admin/stock/get_by_item/'+id,
 			type: "GET",
 			success: function (res) {
 				$('#hidden_stok_edit').val(res.data.quantity);
 				$('#stok_edit').attr('placeholder', res.data.quantity);
 			},
 			error: function (e) {

 			}
 		});

 		// console.log(id);
 	});

 	$('#edit_stok_form').submit(function(e) {
		e.preventDefault();
		var form = $(this);
		var url = form.attr('action');

		$.ajax({
			url: url,
			type: "POST",
			data: form.serialize(),
			success: function(res) {
				form.trigger('reset');
				toastr.success("Data Tersimpan!");
				$('.overlay').addClass('d-flex');
				$('#edit-stok-modal').modal('hide');
				setTimeout(function(){
					$('.overlay').removeClass('d-flex');
					$('.overlay').addClass('d-none');
				}, 2000);
			},
			error: function(e) {
				toastr.info('Gagal Menyimpan Data!');
			}
		}); 
	});

 	// edit user rs btn
 	$('.edit-rs').click(function(e) {
 		e.preventDefault();
 		var id = $(this).attr('data-id');
 		var form = $('#edit_user_form');
 		var url = base_url+'admin/rs/edit';
 		form.attr('action', url+'/'+id);
 		$('#edit-modal').modal('show');

 		$.ajax({
 			url: base_url+'admin/rs/get/'+id,
 			type: "GET",
 			success: function (res) {
 				$('#edit_username').val(res.data.username);
 				$('#edit_f_name').val(res.data.f_name);
 				$('#edit_l_name').val(res.data.l_name);
 				$('#edit_email').val(res.data.email);
 				$('#edit_phone').val(res.data.phone);
 				$('#edit_alamat').val(res.data.address);
 			},
 			error: function (e) {

 			}
 		});

 		console.log(id);
 	});

 	// edit user submit
 	$('#edit_user_form').submit(function(e){
 		e.preventDefault();
 		var form = $(this);
 		var url = form.attr('action');

 		$.ajax({
 			url: url,
 			type: "POST",
 			data: form.serialize(),
 			success: function (res) {
 				toastr.success(res.message);
 				form.trigger('reset');
 				$('#edit-modal').modal('hide');
 				setTimeout(function(){
 					window.location.reload();
 				}, 2000);
 			},
 			error: function (e) {
 				toastr.info(e.message);
 				form.trigger('reset');
 				$('#edit-modal').modal('hide');
 			}
 		});
 	});

 	// edit item

 	$('.edit-item').click(function(e) {
 		e.preventDefault();
 		var id = $(this).attr('data-id');
 		var form = $('#edit_user_form');
 		url = base_url+'admin/item/edit';
 		form.attr('action', url+'/'+id);
 		$('#edit-modal').modal('show');

 		$.ajax({
 			url: base_url+'admin/item/get/'+id,
 			type: "GET",
 			success: function (res) {
 				$('#edit_name').val(res.data.name);
 				$('#edit_category').val(res.data.category);
 				$('#edit_jumlah').val(res.data.stock);
 				$('#edit_deskripsi').val(res.data.description);
 			},
 			error: function (e) {

 			}
 		});

 		console.log(id);
 	});

 	$('.delete').on('click', function (e) {
 		var id = $(this).attr('data-id');
 		var url = $(this).attr('url');
 		$('#conf-modal')
 		.modal({ backdrop: 'static', keyboard: false})
 		.on('click', '#btn-yes', function(e) {
 			console.log(id);
 			del(id, url);
 		});
 	});

 	$('#btn_show_pass').on('click', function (e) {
 		e.preventDefault();
 		$('#pass').attr('type', function(index, attr) {
 			return attr == 'password' ? 'text':'password';
 		});
 	});

 	// submit category
 	$('#add_category_form').submit(function(e) {
 		e.preventDefault();
 		var form = $(this);
 		var url = form.attr('action');

 		$.ajax({
 			url: url,
 			type: "POST",
 			data: form.serialize(),
 			success: function (res) {
 				toastr.success("Data Tersimpan!");
 				form.trigger('reset');
 				setTimeout(function(){
 					window.location.reload();
 				}, 2000);
 			},
 			error: function (err) {
 				toastr.error("Gagal Menyimpan Data!");
 				form.trigger('reset');
 			}
 		});
 	});

 	$('.edit-category').click(function(e) {
 		e.preventDefault();
 		var id = $(this).attr('data-id');
 		var form = $('#edit_user_form');
 		url = base_url+'admin/category/edit';
 		form.attr('action', url+'/'+id);
 		$('#edit-modal').modal('show');

 		$.ajax({
 			url: base_url+'admin/category/get/'+id,
 			type: "GET",
 			success: function (res) {
 				$('#edit_category_name').val(res.data.name);
 				$('#edit_category_deskripsi').val(res.data.description);
 			},
 			error: function (e) {

 			}
 		});

 		console.log(id);
 	});

 	// logout
 	$('#btn_logout').click(function(e) {
 		e.preventDefault();

 		$.ajax({
 			url: base_url+'auth/logout',
 			type: "GET",
 			success: function(e) {
 				console.log(e);
 				if(e.message=="ok") {
 					$(location).attr('href', base_url);
 				}
 			}
 		})
 	}); 

 	// btn proses
 	$('.proses').click(function(e) {
 		e.preventDefault();
 		var id = $(this).attr('data-id');
 		var tipe = $(this).attr('data-tipe')

 		dataPost = {
 			"tipe": tipe
 		};

 		$.ajax({
 			url: base_url+'warehouse/transaction/proses/'+id+'/proses',
 			type: "POST",
 			data: dataPost,
 			success: function (res) {
 				if(res.message=='ok') {
 					toastr.success("Data Diproses!");
 				}else{
 					toastr.error("Gagal diproses!");
 				}
 				setTimeout(function(){
 					window.location.reload();
 				}, 2000);
 			},
 			error: function (err) {
 				toastr.error("Gagal Menyimpan Data!");
 			}
 		});
 	});

 	$('.done').click(function(e){
 		e.preventDefault();
 		var id = $(this).attr('data-id');
 		var itemId = $(this).attr('data-item');
 		var ownerId = $(this).attr('data-owner');
 		var dataPost = {
 			"item_id": itemId,
 			"owner_id": ownerId
 		};
 		$.ajax({
 			url: base_url+'warehouse/transaction/proses_done/'+id+'/done',
 			type: "POST",
 			data: dataPost,
 			success: function (res) {
 				toastr.success("Data Diproses!");
 				setTimeout(function(){
 					window.location.reload();
 				}, 2000);
 			},
 			error: function (err) {
 				toastr.error("Gagal Menyimpan Data!");
 			}
 		});
 	});

 	// autocomplete
 	$('#list_item').autocomplete({
 		minLength: 0,
 		source: function(request, response) {
 			$.ajax({
 				url: base_url+'admin/item/get_all',
 				type: "GET",
 				dataType: "json",
 				data: {
 					term: request.term
 				},
 				success: function(dat) {
 					//console.log(dat);
 					var array = $.map(dat.data, function(item) {
 						return {
 							label: item.name,
 							value: item.id,
 						};
 					});

 					response($.ui.autocomplete.filter(array, request.term));
 				},
 				error: function() {

 				}
 			});
 		},
 		select: function (event, ui) {
 			event.preventDefault();
 			$('#list_item').val(ui.item.label);
	 		$('#item_id').val(ui.item.value);

	 		$.ajax({
	 			url: base_url+'admin/stock/get_by_item/'+ui.item.value,
	 			type: "GET",
	 			dataType: "json",
	 			success: function(e) {
	 				$('#av_stock').val(e.data.quantity);
	 				console.log('av_stock', e.data.quantity);
	 			},
	 			error: function() {
	 				toastr.error('Something Wrong!');
	 			}
	 		});
 		}
 	});

 	$('#donatur').autocomplete({
 		minLength: 0,
 		source: function(request, response) {
 			$.ajax({
 				url: base_url+'admin/donatur/get_donatur',
 				type: "GET",
 				dataType: "json",
 				data: {
 					term: request.term
 				},
 				success: function(dat) {
 					//console.log(dat);
 					var array = $.map(dat.data, function(item) {
 						return {
 							label: item.username,
 							value: item.id,
 						};
 					});

 					response($.ui.autocomplete.filter(array, request.term));
 				},
 				error: function() {

 				}
 			});
 		},
 		select: function (event, ui) {
 			event.preventDefault();
 			$('#donatur').val(ui.item.label);
	 		$('#donatur_id').val(ui.item.value);
	 		
 		}
 	});
 });

function del(id, url) {
	var url = url+id;

	$.ajax({
		url: url,
		type: "post",
		success: function(res) {
			toastr.info('Data dihapus!');
			setTimeout(
			function() {
				window.location.reload(true);
			}, 2000);
		},
		error: function(e) {
			toastr.info('gagal');
		}
	});

	$('#conf-modal').modal('hide');
}
