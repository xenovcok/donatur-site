$(document).ready(function() {
	$('#login_form').submit(function(e) {
		var form = $(this);
		var url = form.attr('action');
		e.preventDefault();

		$.ajax({
			url: url,
			type: "POST",
			data: form.serialize(),
			success: function(res) {
				
				if(res.role == 45) {
					toastr.success('Admin Login berhasil!');
					setTimeout(function(){
						$(location).attr('href', 'admin/dashboard');
					}, 2000);
				}else if(res.role == 46) {
					toastr.success('WH Login berhasil!');
					setTimeout(function(){
						$(location).attr('href', 'warehouse/dashboard');
					}, 2000);
				}else if(res.role == 1) {
					toastr.success('Client Login berhasil!');
					setTimeout(function(){
						$(location).attr('href', 'client/dashboard');
					}, 2000);
				}else{
					toastr.error('Login gagal!');
					form.trigger('reset');
				}

				console.log(res.role);
			},
			error: function(e) {
				toastr.error('Invalid Credentials');
			}
		});
		
	});
});